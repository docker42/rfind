*** Settings ***
Documentation    Tests to verify that anonymous user is redirect on
...              login page when he try to go on GitLab Legacy.
Library           SeleniumLibrary

*** Variables ***
${BROWSER}        firefox

*** Test Cases ***
Go On GitLab Forge And Redirect On Login Page
    Open Browser To GitLab Forge
    Location Should Contain    gitlab.forge.orange-labs.fr/users/sign_in

*** Keywords ***
Open Browser To GitLab Forge
    Open Browser    https://gitlab.forge.orange-labs.fr    ${BROWSER}
    Maximize Browser Window
    Location Should Contain    gitlab.forge.orange-labs.fr
