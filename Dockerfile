FROM ubuntu:18.04

LABEL name="robot-framework"
LABEL description="Robot Framework in Docker"
LABEL url="blabla"
LABEL maintainer=test.test@test.fr

# Setup X Window Virtual Framebuffer
ENV SCREEN_COLOUR_DEPTH 24
ENV SCREEN_HEIGHT 1080
ENV SCREEN_WIDTH 1920

# Default set Robot Framework's threads
ENV ROBOT_THREADS 1

# Prepare scripts to be executed
COPY bin/run-tests-in-virtual-screen.sh /opt/robotframework/bin/run-tests-in-virtual-screen

# Prepare Robot Framework requirements
COPY requirements.txt /tmp/requirements.txt

# Install system dependencies
RUN apt-get update && apt-get install --no-install-recommends -y \
        curl \
        wget \
        python3 \
        python3-pip \
        python3-setuptools \
        xvfb \
        # Chrome packages
        chromium-browser \
        chromium-chromedriver \
        udev \
        # Firefox packages
        firefox \
        firefox-geckodriver \
        dbus \
        fonts-freefont-ttf \
    # Install Robot Framework and Selenium Library (with pip)
    && pip3 install --no-cache-dir -r /tmp/requirements.txt \
    # Remove tmp and cache files
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* ~/.cache/pip

# Update system path
ENV PATH=/opt/robotframework/bin:$PATH

# Robot Framework default user
RUN useradd -ms /bin/bash robot \
    && chmod -R 777 /etc/ssl/certs \
    && chmod -R +x /opt/robotframework/bin
USER robot
WORKDIR /home/robot

HEALTHCHECK NONE

CMD ["run-tests-in-virtual-screen"]