# rfind

Robot Framework in Docker to execute Robot's tests

## Last supported tags and respective Dockerfile links

- latest
- specific


## Deprecated versions

- None

## Usage

In GitLab-CI (to use template see [XXX](https://link.to.tool.com/)) :

```yaml
test:
  image: XXX_IMAGE
  variables:
    ROBOT_TESTS_DIR: "test/example.robot" # Or directory : ROBOT_TESTS_DIR: "test/"
    ROBOT_REPORTS_DIR: "robot-report"
  script:
    - run-tests-in-virtual-screen
  tags:
    - docker
    - rsc
    - shared
```

With CLI (on the host level):

```shell
docker run -it --rm \
  -v <local path to the reports folder>:/opt/robotframework/reports:Z \
  -v <local path to the test suites folder>:/opt/robotframework/tests:Z \
  XXX_IMAGE
```

Enter in the container:

```shell
docker run -it --rm XXX_IMAGE sh
```

## License

XXX


## Contributing

To contribute, create an Issue or a Merge Request.


## Maintainer

Not me
